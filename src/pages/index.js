import { Link } from 'gatsby'
import React from 'react'
import Helmet from 'react-helmet'
import { Waypoint } from 'react-waypoint'
import Header from '../components/Header'
import Measures from '../components/Measures'
import MediaCard from '../components/MediaCard'
import Layout from '../components/layout'
import Nav from '../components/Nav'
import Grid from '@material-ui/core/Grid'
import moment from 'moment'
import coronavirusIllustration from '../assets/images/coronavirus-illustration.png'

class Index extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      stickyNav: false,
      articles: ['Titre 1', 'Titre 2', 'Titre 3', 'Titre 4', 'Titre 5'],
      totalConfirmed: '',
      totalDeaths: '',
      totalRecovered: '',
    }
  }

  _handleWaypointEnter = () => {
    this.setState(() => ({ stickyNav: false }))
  }

  _handleWaypointLeave = () => {
    this.setState(() => ({ stickyNav: true }))
  }
  numberFormat(x) {
    return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, " ");
}
  componentDidMount() {
    let self = this

    var url =
      'https://newsapi.org/v2/top-headlines?' +
      'country=fr&' +
      'q=coronavirus&' +
      'apiKey=1713104a1d5f46e8b2dcc0840d25334d'
    var req = new Request(url)

    fetch(req).then(function(response) {
      response.json().then(function(data) {
        self.setState({ articles: data.articles })
      })
    })

    fetch('https://api.covid19api.com/world/total')
      .then(resp => resp.json())
      .then(function(data) {
        self.setState({
          totalConfirmed: data.TotalConfirmed,
          totalDeaths: data.TotalDeaths,
          totalRecovered: data.TotalRecovered,
        })
      })
      .catch(error => {
        console.log(error)
      })
  }

  render() {
    return (
      <Layout>
        <Helmet title="Covid19 - Informations essentielles" />

        <Header />

        <Waypoint
          onEnter={this._handleWaypointEnter}
          onLeave={this._handleWaypointLeave}
        ></Waypoint>
        <Nav sticky={this.state.stickyNav} />

        <div id="main">
          <section id="intro" className="main">
            <div className="spotlight">
              <div className="content">
                <header className="major">
                  <h2>Coronavirus</h2>
                </header>
                <p>
                  Les coronavirus sont une famille de virus, qui provoquent des
                  maladies allant d’un simple rhume (certains virus saisonniers
                  sont des coronavirus) à des pathologies plus sévères comme les
                  détresses respiratoires du MERS, du SRAS ou du COVID-19. Le
                  virus identifié en janvier 2020 en Chine est un nouveau
                  coronavirus, nommé SARS-CoV-2. La maladie provoquée par ce
                  coronavirus a été nommée COVID-19 par l’Organisation mondiale
                  de la Santé - OMS. Depuis le 11 mars 2020, l’OMS qualifie la
                  situation mondiale du COVID-19 de pandémie ; c’est-à-dire que
                  l’épidémie est désormais mondiale.
                </p>
                <ul className="actions">
                  <li>
                    <Link to="/generic-corona" className="button">
                      En savoir plus
                    </Link>
                  </li>
                </ul>
              </div>
              <span className="image">
                <img src={coronavirusIllustration} alt="" />
              </span>
            </div>
          </section>

          <section id="first" className="main special" style={{"background-color": "whitesmoke"}}>
            <Measures />
          </section>
          <section id="second" className="main special">
            <header className="major">
              <h2>Informations</h2>
              <p>
                Statistiques dans le <b>monde</b> au <b>{moment().format('DD/MM/YYYY')}</b> :
              </p>
            </header>
            <ul className="statistics">
            <li style={{"background-color": "#3663a3"}}>
                <span className="icon fa-signal"></span>
                <strong style={{"fontWeight": "500"}}>{this.numberFormat(this.state.totalConfirmed)}</strong> Personnes infectées
              </li>
              <li style={{"background-color": "#e37682"}}>
                <span className="icon fa-signal"></span>
                <strong style={{"fontWeight": "500"}}>{this.numberFormat(this.state.totalDeaths)}</strong> Personnes décédées
              </li>
              <li style={{"background-color": "#3663a3"}}>
                <span className="icon fa-signal"></span>
                <strong style={{"fontWeight": "500"}}>{this.numberFormat(this.state.totalRecovered)}</strong> Personnes rétablies
              </li>
            </ul>
            <footer className="major">
              <ul className="actions">
                <li>
                  <Link to="/generic-corona" className="button">
                    En savoir plus
                  </Link>
                </li>
              </ul>
            </footer>
          </section>

          <section id="cta" className="main special" style={{"background-color": "whitesmoke"}}>
            <header className="major">
              <h2>Actualités</h2>
              <Grid container spacing={2} style={{ flexWrap: 'nowrap' }}>
                <Grid item xs={12}>
                  <Grid container justify="center">
                    {this.state.articles.map((article, key) => {
                      if (article.urlToImage) {
                        return (
                          <Grid style={{ margin: '10px' }} key={key} item>
                            <MediaCard
                              image={article.urlToImage}
                              title={article.title}
                              description={article.description}
                              url={article.url}
                            />
                          </Grid>
                        )
                      }
                    })}
                  </Grid>
                </Grid>
              </Grid>
            </header>
            <footer className="major">
              <ul className="actions">
                <li>
                  <Link to="/generic-corona" className="button">
                    En savoir plus
                  </Link>
                </li>
              </ul>
            </footer>
          </section>
        </div>
      </Layout>
    )
  }
}

export default Index
