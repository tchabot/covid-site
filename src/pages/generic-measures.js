import React from 'react'
import Helmet from 'react-helmet'

import Layout from '../components/layout'
import HeaderGeneric from '../components/HeaderGeneric'
import pic04 from '../assets/images/pic04.jpg'

class GenericMeasures extends React.Component {
  render() {
    return (
      <Layout>
        <Helmet title="Generic Page Title" />
        <HeaderGeneric title="Les mesures à appliquer" />
        <div id="main">
          <section id="content" className="main">
            <span className="image main">
              <img style={{ maxHeight: '100px' }} src={pic04} alt="" />
            </span>
            <h2>Je n'ai pas de symptômes</h2>
            <h3>Je ne vis pas avec un cas COVID-19</h3>
            <p>
              <ul>
                <li>Je reste confiné chez moi</li>
                <li>Je respecte la consigne de distanciation</li>
                <li>
                  Je respecte les gestes simples pour me protéger et protéger
                  mon entourage
                </li>
                <li>
                  Je ne sors que pour le travail et l’approvisionnement
                  alimentaire
                </li>
                <li>Je favorise le télétravail si disponible</li>
              </ul>
            </p>
            <h3>Je vis avec un cas COVID-19</h3>
            <p>
              <ul>
                <li>Je reste à mon domicile et je m’isole</li>
                <li>
                  Je respecte les gestes simples pour me protéger et protéger
                  mon entourage
                </li>
                <li>
                  Je surveille ma température 2 fois par jour et l’apparition de
                  symptômes (toux, fièvre, difficultés respiratoires)
                </li>
                <li>Je suis arrêté sauf si le télétravail est disponible</li>
                <li>
                  Si je suis personnel de santé, je poursuis le travail avec un
                  masque
                </li>
              </ul>
            </p>
            <h2>J'ai des symptômes (toux, fièvre)</h2>
            <h3>Je tousse et/ou j’ai de la fièvre</h3>
            <p>
              <ul>
                <li>
                  J’appelle un médecin (médecin traitant, téléconsultation)
                </li>
                <li>Je reste à mon domicile et je m’isole</li>
              </ul>
            </p>
            <h3>Je tousse et j’ai de la fièvre. J’ai du mal à respirer et/ou j’ai fait un malaise</h3>
            <p>
              J'appelle le 15
            </p>
          </section>
        </div>
      </Layout>
    )
  }
}

export default GenericMeasures
