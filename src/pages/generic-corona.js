import React from 'react'
import Helmet from 'react-helmet'

import Layout from '../components/layout'
import HeaderGeneric from '../components/HeaderGeneric'
import pic04 from '../assets/images/pic04.jpg'

class Generic extends React.Component {
  render() {
    return (
      <Layout>
        <Helmet title="Generic Page Title" />
        <HeaderGeneric title="Corona"/>
        <div id="main">
          <section id="content" className="main">
            <span className="image main">
              <img style={{"maxHeight": "100px"}} src={pic04} alt="" />
            </span>
            <h2>D'où vient le coronavirus ?</h2>
            <p>
              Les premières personnes à avoir contracté le virus s’étaient
              rendues au marché de Wuhan dans la Province de Hubei en Chine. La
              maladie semblerait donc venir d’un animal (zoonose) mais l’origine
              exacte n’a pas été confirmée.
            </p>
            <h2>Quelle est la dangerosité du coronavirus ?</h2>
            <p>
              Le coronavirus est dangereux pour trois raisons : Il est très
              contagieux : chaque personne infectée va contaminer au moins 3
              personnes en l'absence de mesures de protection. Il est contagieux
              avant d’être symptomatique, c’est-à-dire qu’une personne
              contaminée, mais qui ne ressent pas encore de symptômes, peut
              contaminer d’autres personnes. Environ 15% des cas constatés ont
              des complications et 5% ont besoin d'être hospitalisés en
              réanimation.
            </p>
          </section>
        </div>
      </Layout>
    )
  }
}

export default Generic
