import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing(2),
    margin: 'auto',
    maxWidth: 400,
  },
  img: {
    margin: 'auto',
    display: 'block',
    width: '100%',
    maxHeight: '100%',
  },
  h2: {
    fontSize: '1.2em !important'
  },
  body: {
    fontSize: '0.8em !important'
  }
}));

export default function ComplexGrid(props) {
  const classes = useStyles();

  return (
    <a target="_blank" rel="noopener noreferrer" href={props.url}>
      <div className={classes.root}>
        <Paper className={classes.paper}>
          <Grid container spacing={2}>
            <Grid item>
              <img className={classes.img} alt="complex" src={props.image} />
            </Grid>
            <Grid item xs={12} sm container>
              <Grid item xs container direction="column" spacing={2}>
                <Grid item xs>
                  <Typography gutterBottom variant="h2" component="h2" className={classes.h2}>
                    {props.title}
                  </Typography>
                  <Typography variant="body2" gutterBottom className={classes.body}>
                    {props.description}
                  </Typography>
                </Grid>
              </Grid>
            </Grid>
          </Grid>
        </Paper>
      </div>
    </a>
  );
}