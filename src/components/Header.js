import React from 'react'

import logo from '../assets/images/logo-covid.svg'

const styles = {
  logo: {
    maxHeight: '350px',
  },
}

const Header = props => (
  <header id="header" className="alt">
    <span className="logo">
      <img style={styles.logo} src={logo} alt="" />
    </span>
  </header>
)

export default Header
