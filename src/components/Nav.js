import React from 'react'
import Scrollspy from 'react-scrollspy'
import Scroll from './Scroll'
import '../../src/assets/scss/style.css'
const Nav = (props) => (
    <nav id="nav" className={props.sticky ? 'alt' : ''}>
        <Scrollspy items={ ['intro', 'first', 'second', 'cta'] } currentClassName="is-active" offset={-300}>
            <li>
                <Scroll type="id" element="intro">
                    <a href="#">Le Covid-19</a>
                </Scroll>
            </li>
            <li>
                <Scroll type="id" element="first">
                    <a href="#">Les mesures à appliquer</a>
                </Scroll>
            </li>
            <li>
                <Scroll type="id" element="second">
                    <a href="#">Informations</a>
                </Scroll>
            </li>
            <li>
                <Scroll type="id" element="cta">
                    <a href="#">Les actualités</a>
                </Scroll>
            </li>
        </Scrollspy>
    </nav>
)

export default Nav
