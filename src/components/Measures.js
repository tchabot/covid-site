import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import eloigne from '../assets/images/messages_barriere/eloigne.svg'
import mouchoire from '../assets/images/messages_barriere/mouchoire.svg'
import tousser_coude from '../assets/images/messages_barriere/tousser_coude.svg'
import laver_les_mains from '../assets/images/messages_barriere/laver_les_mains.svg'
import distanciation from '../assets/images/messages_barriere/Distanciation.svg'
import masque from '../assets/images/messages_barriere/masque.svg'
import attestation from '../assets/images/messages_barriere/attestation.svg'
import Divider from '@material-ui/core/Divider';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    paper: {
        padding: theme.spacing(2),
        textAlign: 'center',
        color: theme.palette.text.secondary,
    },
    subtitle: {
        fontWeight: "700"
    },
    title: {
        fontSize: "1.3em",
        fontWeight: "700",
        textDecoration: "underline"
    },
    important: {
        marginTop: "100px",
    },
    secondPart: {
        marginTop: "25px",
        marginBottom: "10px"
    }
}));

export default function FullWidthGrid() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Grid container spacing={3}>
                <Grid item xs={12}>
                    <header className="major">
                        <h2>Les mesures à appliquer</h2>
                    </header>
                </Grid>
                <Grid item xs={12} sm={3}>
                    <img src={laver_les_mains} />
                </Grid>

                <Grid item xs={12} sm={3}>
                    <img src={tousser_coude} />
                </Grid>
                <Grid item xs={12} sm={3}>
                    <img src={mouchoire} />
                </Grid>
                <Grid item xs={12} sm={3}>
                    <img src={eloigne} />
                </Grid>
                <Grid className={classes.subtitle} item xs={12} sm={3}>
                    Se laver très régulièrement les mains
                </Grid>
                <Grid className={classes.subtitle} item xs={12} sm={3}>
                    Tousser ou éternuer dans son coude ou dans un mouchoir
                </Grid>
                <Grid className={classes.subtitle} item xs={12} sm={3}>
                    Utiliser un mouchoir à usage unique et le jeter
                </Grid>
                <Grid className={classes.subtitle} item xs={12} sm={3}>
                    Saluer sans se serrer la main, éviter les embrassades
                </Grid>

            </Grid>

            <Grid container spacing={8} className={classes.important} justify="center">
                <Grid container spacing={8}>
                    <Grid className={classes.title} item xs={12} sm={12}>
                        Mais aussi si vous sortez ...
                    </Grid>
                    <Grid className={classes.subtitle} item xs={12} sm={9}>
                        <img src={distanciation} />
                    </Grid>
                    <Grid alignContent="center" alignItems="center" justify="center" container className={classes.subtitle} item xs={12} sm={2}>
                        Rester à 1m de toutes autres personnes
                    </Grid>
                </Grid>

                <Grid container spacing={8} className={classes.secondPart} justify="center">
                    <Grid alignContent="center" alignItems="center" justify="center" container className={classes.subtitle} item xs={12} sm={3}>
                        <img src={masque} />
                    </Grid>
                    <Grid alignContent="center" alignItems="center" justify="center" container className={classes.subtitle} item xs={12} sm={3}>
                        Portez un masque en tissus si possible
                    </Grid>

                    <Divider orientation="vertical" flexItem />

                    <Grid alignContent="center" alignItems="center" justify="center" container className={classes.subtitle} item xs={12} sm={2}>
                        <img width="110px" src={attestation} />
                    </Grid>
                    <Grid alignContent="center" alignItems="center" justify="center" container className={classes.subtitle} item xs={12} sm={3}>
                        Jusqu'au 11 Mai 2020, prenez votre attestation de déplacement
                    </Grid>
                </Grid>
            </Grid>
        </div>
    );
}