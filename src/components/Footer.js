import { Link } from 'gatsby'
import React from 'react'

const Footer = props => (
  <footer id="footer">
    <section>
      <h2>A propos</h2>
      <p>
        Fait dans le cadre d'un projet Campus Academy.
      </p>
    </section>
  </footer>
)

export default Footer
